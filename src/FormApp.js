import React, { Component } from 'react';
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Values } from "redux-form-website-template";
import store from "./helpers/store";
import showResults from "./helpers/showResults";
import UserProfile from "./components/UserProfile/UserProfile";
import './bundle.css';

class FormApp extends Component {
    render() {
        return (
          <Provider store={store}>
            <div style={{ padding: 15 }}>
              <h2>User Profile Information</h2>
              <UserProfile onSubmit={showResults} />
              <Values form="simple" />
            </div>
          </Provider>
        );
    }
}

export default FormApp;
