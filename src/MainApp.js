import React from 'react'
import Main from './Main'

const MainApp = () => (
  <div>
    <Main />
  </div>
)

export default MainApp
